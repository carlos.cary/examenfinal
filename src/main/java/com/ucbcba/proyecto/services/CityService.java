package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.City;

public interface CityService {

    Iterable<City> listAllCitys();

    City getCityById(Integer id);

    City saveCity(City city);

    void deleteCity(Integer id);

}
