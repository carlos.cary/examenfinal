package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.City;
import com.ucbcba.proyecto.entities.Country;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CountryRepository extends CrudRepository<Country,Integer> {

}
