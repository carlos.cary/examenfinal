package com.ucbcba.proyecto.controllers;

import com.ucbcba.proyecto.entities.Country;
import com.ucbcba.proyecto.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class CountryController {


    private CountryService countryService;

    @RequestMapping(value = "/admin/newCountry", method = RequestMethod.GET)
    public String newCountry(Model model) {
        model.addAttribute("country", new Country());

        return "registration";//implementar html registration Carlos
    }

    @RequestMapping(value = "/admin/newCountry", method = RequestMethod.POST)
    public String saveCountry(@Valid Country country, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute(country);
            return "/admin/newCountry";
        }
        countryService.saveCountry(country);
        return "redirect:/admin/home";
    }

    @RequestMapping(value = "/admin/country/editar/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable Integer id, Model model) {
        Country country = countryService.getCountryById(id);
        model.addAttribute("country", country);
        return "/admin/newCountry";
    }

    @RequestMapping(value = "/admin/country/eliminar/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable Integer id, Model model) {
        countryService.deleteCountry(id);
        return "redirect:/admin/home";
    }

    @RequestMapping(value = "/admin/countries", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("countries", countryService.listAllCountries());
        return "countries";
    }
}
